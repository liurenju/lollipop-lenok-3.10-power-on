cmd_arch/arm/boot/dts/apq8026-lenok-rev-10.dtb := /home/liurenju/Desktop/wear_research/Lollipop_kernel/msm/scripts/gcc-wrapper.py arm-eabi-gcc -E -Wp,-MD,arch/arm/boot/dts/.apq8026-lenok-rev-10.dtb.d.pre.tmp -nostdinc -I/home/liurenju/Desktop/wear_research/Lollipop_kernel/msm/arch/arm/boot/dts -I/home/liurenju/Desktop/wear_research/Lollipop_kernel/msm/arch/arm/boot/dts/include -undef -D__DTS__ -x assembler-with-cpp -o arch/arm/boot/dts/.apq8026-lenok-rev-10.dtb.dts.tmp arch/arm/boot/dts/apq8026-lenok-rev-10.dts ; /home/liurenju/Desktop/wear_research/Lollipop_kernel/msm/scripts/dtc/dtc -O dtb -o arch/arm/boot/dts/apq8026-lenok-rev-10.dtb -b 0 -i arch/arm/boot/dts/  -d arch/arm/boot/dts/.apq8026-lenok-rev-10.dtb.d.dtc.tmp arch/arm/boot/dts/.apq8026-lenok-rev-10.dtb.dts.tmp ; cat arch/arm/boot/dts/.apq8026-lenok-rev-10.dtb.d.pre.tmp arch/arm/boot/dts/.apq8026-lenok-rev-10.dtb.d.dtc.tmp > arch/arm/boot/dts/.apq8026-lenok-rev-10.dtb.d

source_arch/arm/boot/dts/apq8026-lenok-rev-10.dtb := arch/arm/boot/dts/apq8026-lenok-rev-10.dts

deps_arch/arm/boot/dts/apq8026-lenok-rev-10.dtb := \
  arch/arm/boot/dts/apq8026-lenok/apq8026-lenok-rev-10.dtsi \
  arch/arm/boot/dts/apq8026-lenok/apq8026-lenok.dtsi \
  arch/arm/boot/dts/apq8026-lenok/../qcom/msm8226.dtsi \
  arch/arm/boot/dts/apq8026-lenok/../qcom/skeleton.dtsi \
  arch/arm/boot/dts/apq8026-lenok/../qcom/msm8226-camera.dtsi \
  arch/arm/boot/dts/apq8026-lenok/../qcom/msm-gdsc.dtsi \
  arch/arm/boot/dts/apq8026-lenok/../qcom/msm8226-iommu.dtsi \
  arch/arm/boot/dts/apq8026-lenok/../qcom/msm-iommu-v1.dtsi \
  arch/arm/boot/dts/apq8026-lenok/../qcom/msm8226-smp2p.dtsi \
  arch/arm/boot/dts/apq8026-lenok/../qcom/msm8226-gpu.dtsi \
  arch/arm/boot/dts/apq8026-lenok/../qcom/msm8226-bus.dtsi \
  arch/arm/boot/dts/apq8026-lenok/../qcom/msm8226-mdss.dtsi \
  arch/arm/boot/dts/apq8026-lenok/../qcom/msm8226-mdss-panels.dtsi \
  arch/arm/boot/dts/apq8026-lenok/../qcom/dsi-panel-hx8394a-720p-video.dtsi \
  arch/arm/boot/dts/apq8026-lenok/../qcom/dsi-panel-nt35590-720p-video.dtsi \
  arch/arm/boot/dts/apq8026-lenok/../qcom/dsi-panel-nt35521-720p-video.dtsi \
  arch/arm/boot/dts/apq8026-lenok/../qcom/dsi-panel-nt35596-1080p-video.dtsi \
  arch/arm/boot/dts/apq8026-lenok/../qcom/dsi-panel-nt35590-720p-cmd.dtsi \
  arch/arm/boot/dts/apq8026-lenok/../qcom/dsi-panel-ssd2080m-720p-video.dtsi \
  arch/arm/boot/dts/apq8026-lenok/../qcom/dsi-panel-jdi-1080p-video.dtsi \
  arch/arm/boot/dts/apq8026-lenok/../qcom/dsi-panel-nt35590-qvga-cmd.dtsi \
  arch/arm/boot/dts/apq8026-lenok/../qcom/msm8226-coresight.dtsi \
  arch/arm/boot/dts/apq8026-lenok/../qcom/msm8226-iommu-domains.dtsi \
  arch/arm/boot/dts/apq8026-lenok/../qcom/msm8226-pinctrl.dtsi \
  arch/arm/boot/dts/apq8026-lenok/../qcom/msm-rdbg.dtsi \
  arch/arm/boot/dts/apq8026-lenok/../qcom/msm-pm8226-rpm-regulator.dtsi \
  arch/arm/boot/dts/apq8026-lenok/../qcom/msm-pm8226.dtsi \
  arch/arm/boot/dts/apq8026-lenok/../qcom/msm8226-regulator.dtsi \
  arch/arm/boot/dts/apq8026-lenok/../qcom/apq8026-v2.dtsi \
  arch/arm/boot/dts/apq8026-lenok/../qcom/msm8226-v2.dtsi \
  arch/arm/boot/dts/apq8026-lenok/../qcom/msm8226.dtsi \
  arch/arm/boot/dts/apq8026-lenok/../qcom/msm8226-v2-pm.dtsi \
  arch/arm/boot/dts/apq8026-lenok/apq8026-lenok-memory.dtsi \
  arch/arm/boot/dts/apq8026-lenok/../qcom/msm8226-w-ion.dtsi \
  arch/arm/boot/dts/apq8026-lenok/../qcom/msm8226-w-qseecom.dtsi \
  arch/arm/boot/dts/apq8026-lenok/../qcom/msm8226-v2-pm.dtsi \
  arch/arm/boot/dts/apq8026-lenok/apq8026-lenok-panel.dtsi \
  arch/arm/boot/dts/apq8026-lenok/apq8026-lenok-sensor.dtsi \
  arch/arm/boot/dts/apq8026-lenok/apq8026-lenok-usb.dtsi \
  arch/arm/boot/dts/apq8026-lenok/apq8026-lenok-misc.dtsi \
  arch/arm/boot/dts/apq8026-lenok/apq8026-lenok-pm.dtsi \
  arch/arm/boot/dts/apq8026-lenok/apq8026-lenok-input.dtsi \
  arch/arm/boot/dts/apq8026-lenok/apq8026-lenok-bt.dtsi \
  arch/arm/boot/dts/apq8026-lenok/apq8026-lenok-sound.dtsi \

arch/arm/boot/dts/apq8026-lenok-rev-10.dtb: $(deps_arch/arm/boot/dts/apq8026-lenok-rev-10.dtb)

$(deps_arch/arm/boot/dts/apq8026-lenok-rev-10.dtb):
